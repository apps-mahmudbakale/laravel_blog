<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
	//Table Name
    // protected $table = 'posts';

    // //Primary Key
    // public $primaryKey = 'id';

    // //Time Stamp
    // public $timestamps = true;

    protected $fillable = [
    'title',
    'body',
    'user_id'
   ];

    public function user()
    {
    	return $this->belongsTo('App\User');
    }


    public function comments()
    {
        return $this->hasMany('App\Comment');
    }


    public function dashboard()

    {
        return $this->belongsTo('App\Dashboard');
    }
}
