@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <a href="{{ route('posts.create') }}" class="btn btn-primary"><i class="fa fa-pencil"></i> Create Post</a>
                   <h1>My Posts</h1>
                        @if(filled($posts))
                    <table class="table table-striped">
                        <thead>
                            <th>Title</th>
                            <th></th>
                            <th></th>
                        </thead>

                        <tbody>
                            @foreach($posts as $post)

                            <tr>
                                <td>{{ $post->title }}</td>
                                <td><a href="/posts/{{ $post->id }}/edit" class="btn btn-primary"><i class="fa fa-edit"></i> Edit</a></td>
                                <td><a href="" class="btn btn-danger"><i class="fa fa-trash-o"></i> Delete</a></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    @else
                    <h3>You Have No Post</h3>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
