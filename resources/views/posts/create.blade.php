@extends('layouts.app')

@section('content')
    <div class="container">
            <h2>Make A New Post</h2>
              <hr>
               <form action="{{ route('posts.store') }}" method="POST">
                 @csrf
                 <div class="col-md-12">
                   Title
                   <input type="text" name="title" class="form-control">
                 </div>
                  <div class="col-md-12">
                   Post Body
                   <textarea name="body" id="" cols="30" rows="10" class="form-control"></textarea>
                 </div>
                  <div class="col-md-6">
                   <br>
                  <button type="submit" name="submit" class="btn btn-success">Submit</button>
                 </div>
               </form>              
        </div>
  @endsection