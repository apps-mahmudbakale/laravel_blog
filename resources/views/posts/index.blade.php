@extends('layouts.app')

@section('content')
    <div class="container">
             @guest
                    @if (Route::has('register'))
                       
                    @endif
                @else
                   <a href="{{ route('posts.create') }}" class="btn btn-primary"><i class="fa fa-pencil"></i> Create A Post</a>
                    <br>
                   <br>
                @endguest
            
                    @if(filled($posts))
                      
                      @foreach($posts as $key => $post)
                        <div class="col-md-12">
                          <div class="card border-success mb-3">
                            <div class="card-header bg-transparent border-success"><h1>{{ $post->title }}</h1></div>
                            <div class="card-body text-success">
                               <img src="{{ asset('images/blog.png') }}" alt="post img" class="img-thumbnail float-left">
                              <p class="card-text text-justify">{{ $post->body }}</p>
                            </div>
                            <div class="card-footer bg-transparent border-success">Posted On <i class="fa fa-clock-o"></i> {{ $post->created_at->diffForHumans()}} By {{ $post->user->name }} <a href="/posts/{{ $post->id }}" class="btn btn-success pull-right">Read More</a> </div>
                          </div>
                        </div>   
                       

                      @endforeach

                    @endif


    </div>
  @endsection