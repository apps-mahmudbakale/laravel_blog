@extends('layouts.app')

@section('content')
    <div class="container">
             @guest
                    @if (Route::has('register'))
                       
                    @endif
                @else
                   <a href="{{ route('posts.create') }}" class="btn btn-primary"><i class="fa fa-pencil"></i> Create A Post</a>
                   <br>
                   <br>
                @endguest

                        <div class="col-md-12">
                          <div class="card border-success mb-3">
                            <div class="card-header bg-transparent border-success"><h1>{{ $post->title }}</h1></div>
                            <div class="card-body text-success">
                               <img src="{{ asset('images/blog.png') }}" alt="post img" class="img-thumbnail float-left">
                              <p class="card-text text-justify">{{ $post->body }}</p>
                            </div>
                            <div class="card-footer bg-transparent border-success">Posted On <i class="fa fa-clock-o"></i> {{ $post->created_at->diffForHumans()}} By {{ $post->user->name }} </div>
                          </div>
                          <hr>

                          <div class="card border-success mb-3">
                            <div class="card-header bg-transparent border-success"><h1><i class="fa fa-comments"></i> Comments</h1></div>
                            <div class="card-body text-success">
                              <ul class="list-group">
                              @foreach($post->comments as $comment)
                                <strong>{{ $comment->user->name }}:   <span class="pull-right"><i class="fa fa-clock-o"> {{ $comment->created_at->diffForHumans() }}</i></span></strong>

                                  <li class="list-group-item">
                                    {{ $comment->body }}
                                  </li>

                              @endforeach
                            </ul>
                            </div>
                            <div class="card-footer bg-transparent border-success">
                              @guest
                                     @if (Route::has('register'))
                                        
                                     @endif
                                 @else
                                   <form action="\posts\{{$post->id }}\comments" method="POST">
                                    @csrf
                                    <div class="col-md-12">
                                      Type Your Comment
                                      <textarea name="comment" id="" cols="30" rows="10" class="form-control"></textarea>
                                    </div>
                                    <div class="col-md-6">
                                      <br>
                                      <button type="submit" name="submit" class="btn btn-success">Submit</button>
                                    </div>
                                  </form>
                                 @endguest
                            </div>
                          </div>

                        </div>   

    </div>
  @endsection